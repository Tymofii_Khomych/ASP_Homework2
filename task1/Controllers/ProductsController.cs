﻿using Microsoft.AspNetCore.Mvc;
using task1.Models;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Eventing.Reader;

namespace task1.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ProductReader _reader;

        public ProductsController()
        {
            _reader = new ProductReader();
        }

        [Route("products/list/{category?}")]
        public IActionResult List(string? category)
        {
            List<Product> products = _reader.ReadFromFile();
            List<Product> productInCategory = new List<Product>();

            foreach (var product in products)
            {
                if (product.Category == category)
                {
                    productInCategory.Add(product);
                }
            }

            if (category == null)
            {
                return View(products as object);
            }
            else if(productInCategory.Count != 0)
            {
                return View(productInCategory as object);
            }
            else
            {
                return NotFound();
            }         
        }

        [Route("products/details/{id}")]
        public IActionResult Details(int id)
        {
            List<Product> products = _reader.ReadFromFile();
            Product product = products.Where(x => x.Id == id).FirstOrDefault();

            if (product != null)
            {
                return View(product);
            }
            else
            {
                return NotFound();
            }
        }
    }
}