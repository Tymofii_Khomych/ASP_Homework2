﻿using Microsoft.AspNetCore.Mvc;

namespace task2.Controllers
{
    public class CalcController : Controller
    {
        public IActionResult Add(int x, int y)
        {
            int result = x + y;
            return View("Calc", result);
        }

        public IActionResult Sub(int x, int y)
        {
            int result = x - y;
            return View("Calc", result);
        }

        public IActionResult Mul(int x, int y)
        {
            int result = x * y;
            return View("Calc", result);
        }

        public IActionResult Div(int x, int y)
        {
            double result = (double)x / y;
            return View("Calc", result);
        }
    }
}
